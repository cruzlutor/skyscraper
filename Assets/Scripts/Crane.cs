﻿using UnityEngine;
using System.Collections;

public class Crane : MonoBehaviour {

	/* flag to mark if the crane can shot or not */
	private bool busy = false;

	/* flag to mark if the crane can move or not */
	private bool move = true;

	/* flag to mark if the crane can move to right or left */
	private bool dirRight = true;

	/* flag to mark if the crane is active or not */
	public bool active = true;

	/* crane movement speed */
	public float speed = 2.0f;

	/* current floor in the crane */
	private Transform newFloor;
	
	/* floor prefab */
	public Transform floor;
	
	/* build gameobject */
	public Transform build;


	// Use this for initialization
	void Start () 
	{
		Reload ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		Move ();
	}
	
	/* this function is invoked when the game is losed */
	public void Lose()
	{
		active = false;
		//Release ();
	}

	/* this function is invoked when the game is restarted */
	/*
	public void Retry()
	{
		active = true;
		//Reload ();
	}
	*/

	/* crane move logic */
	void Move()
	{

		if (move == true) {
			if (dirRight)
				transform.Translate (Vector2.right * speed * Time.deltaTime);
			else
				transform.Translate (-Vector2.right * speed * Time.deltaTime);
			
			if(transform.position.x >= 4.5f) {
				dirRight = false;
			}
			
			if(transform.position.x <= -4.5f) {
				dirRight = true;
			}
		}
	}

	/* this function make a floor reload in the crane */
	public void Reload()
	{
		busy = false;
		Vector2 pos = new Vector2 (transform.position.x, transform.position.y - 2f);
		newFloor = Instantiate (floor, pos, transform.rotation) as Transform;
		newFloor.parent = transform;

	}

	/* this function shoot the current floor in the crane */
	public void Shot()
	{
		if (busy == false) 
		{
			busy = true;
			Rigidbody2D body = newFloor.GetComponent<Rigidbody2D> ();
			body.constraints = RigidbodyConstraints2D.None;

			/* assign the current floor to the build gameobject */
			newFloor.parent = build;

			build.gameObject.GetComponent<Build> ().OnNewFloor ();

			/* declare delay */
			StartCoroutine (DelayMove (0.5f));
			StartCoroutine (DelayReload (1.0f));
		}
	}

	/* delay the crane move */
	private IEnumerator DelayMove(float time) 
	{
		move = false;
		yield return new WaitForSeconds(time);
		move = true;
		yield return null;
	}
	
	/* delay the crane reload */
	private IEnumerator DelayReload(float time) 
	{
		yield return new WaitForSeconds(time);
		Reload ();
		yield return null;
	}
}
