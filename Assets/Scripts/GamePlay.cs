﻿using UnityEngine;
using System.Collections;

public class GamePlay : MonoBehaviour {

	public GameObject build;
	public GameObject crane;
	public GameObject pauseMenu;
	public GameObject gameOverMenu;
	public GameObject HUD;
	public GameObject parallax; 
	public bool lose;
	public bool pause;

	// Use this for initialization
	void Start (){
		lose = false;
		pause = false;
	}
	
	// Update is called once per frame
	void Update (){
		if (lose == false && pause == false){
			if (Input.GetMouseButtonDown (0)){
				Vector3 pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
				if (hit) {
					if(hit.collider.name != "PauseButton"){
						crane.GetComponent<Crane>().Shot();
					}
				}else{
					crane.GetComponent<Crane>().Shot();
				}
			}
		}
	}

	public void Lose(){
		build.GetComponent<Build> ().Lose ();
		parallax.GetComponent<Parallax> ().Lose ();
		gameOverMenu.SetActive (true);
		lose = true;
	}
	
	public void Pause(){
		if (lose == false) {
			pauseMenu.SetActive (true);
			pause = true;
		}
	}
	
	public void Resume(){
		pauseMenu.SetActive (false);
		pause = false;
	}

	public void Retry(){
		//PlayerPrefs.DeleteAll ();
		//ScoreManager.Retry ();
		Application.LoadLevel ("Main");
		/*
		build.GetComponent<Build> ().Retry ();
		crane.GetComponent<Crane> ().Retry ();
		parallax.GetComponent<Parallax> ().Retry ();
		*/
		//menu.SetActive (false);
		//lose = false;
		//pause = false;
	}

	public void Menu(){
		Application.LoadLevel ("Menu");
	}

	public void Scrolling(){
		if (lose == false){
			parallax.GetComponent<Parallax> ().StartScrolling ();
		}
	}
}
