﻿using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour {


	public Transform crane;
	public bool move = false;
	private Vector3 next = new Vector3(0,0,0);
	private Vector3 cameraPos;
	private Transform camera;

	// Use this for initialization
	void Start () {
		camera = Camera.main.transform;
		cameraPos = camera.position;
	}
	
	public void Lose(){
		StopScrolling ();
	}

	// Update is called once per frame
	void Update () {
		if (move == true) {
			camera.position = Vector3.MoveTowards(camera.position, next, (2 * Time.deltaTime));

			foreach (Transform child in transform)
			{
				child.gameObject.GetComponent<ParallaxLayer> ().active = true;
			}

			if (camera.position == next) {
				StopScrolling();
			}
		}
	}

	public void StartScrolling()
	{
		next = new Vector3 (camera.position.x, camera.position.y + 1.23f, camera.position.z);
		move = true;
	}
	
	public void StopScrolling()
	{
		foreach (Transform child in transform)
		{
			child.gameObject.GetComponent<ParallaxLayer> ().active = false;
		}
		move = false;
	}
	/*
	public void Retry(){
		camera.position = cameraPos;
		foreach (Transform child in transform)
		{
			child.gameObject.GetComponent<ParallaxLayer>().Retry();
		}
	}
	*/
}
