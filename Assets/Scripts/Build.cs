﻿using UnityEngine;
using System.Collections;

public class Build : MonoBehaviour {

	/* flag to mark if the crane can update or not */
	public bool update = true;
	public GameObject line;
	public GameObject controller;
	public GameObject parallax;

	//private Vector3 next = new Vector3(0,0,0);

	// Use this for initialization
	void Start (){
		DrawLine ();
	}

	void Update (){
		if (update == true) {
			CheckFall();
		}
	}

	void DrawLine(){
		float high = ScoreManager.GetHighPosition ();
		if(high != 0){
			line.SetActive (true);
			line.transform.position = new Vector2(line.transform.position.x, high);
		}
	}

	public void Lose(){
		update = false;
	}

	/*
	public void Retry()
	{
		for(int i = transform.childCount - 1; i >= 0; i--)
		{
			DestroyImmediate(transform.GetChild(i).gameObject);
		}
		update = true;
		ScoreManager.SetFloors (0);
		DrawLine ();
	}
	*/

	public void CheckFall(){
		if (update == true){
			if (transform.childCount > 1){
				transform.GetChild (transform.childCount-1).GetComponent<Floor> ().Check();
			}
		}
	}

	/*
	public void OnCollision(Transform obj1, Transform obj2){
		//if (obj1.position.y > obj2.position.y){
			CheckHigh(obj1);

			float result = Mathf.Abs(obj1.position.x - obj2.position.x);
			if(result < 0.1){
				//AddCombo();
				//obj2.GetComponent<Rigidbody2D>().freezeRotation = true;
			}else{
				//ResetCombo();
			}

		//}
	}*/


	public void CheckHigh(){
		/* on collision add the last child Y position to high Y */
		if (update == true) {
			if (ScoreManager.GetHighScore () < ScoreManager.floors) {
				ScoreManager.SetHighScore (ScoreManager.floors);
				Transform lastFloot = transform.GetChild (transform.childCount - 1);
				ScoreManager.SetHighPosition (lastFloot.position.y + lastFloot.lossyScale.y);
			}
		}
	}

	public void floorFall(){
		controller.GetComponent<GamePlay>().Lose();
	}
	
	public void OnNewFloor(){
		ScoreManager.AddFloor();
		if (transform.childCount > 1){
			transform.GetChild (transform.childCount-1).GetComponent<Floor> ().limit = transform.GetChild (transform.childCount-2).position.y;
		}
		if (transform.childCount > 2){
			controller.GetComponent<GamePlay>().Scrolling();
		}
	}
}