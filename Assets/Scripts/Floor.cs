﻿using UnityEngine;
using System.Collections;

public class Floor : MonoBehaviour {

	public bool grounded = false;
	public float limit = 0f;

	// Use this for initialization
	void Start () {

		SpriteRenderer sprite =  GetComponent<SpriteRenderer>();
		sprite.sortingLayerName = "Build";

		gameObject.tag = "Floor";
	
		Rigidbody2D body = GetComponent<Rigidbody2D>();

		//body.mass = body.mass - (ScoreManager.floors * 0.1f);
		//print (body.mass);
		body.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;

	}
	
	// Update is called once per frame
	void Update () {
		if (grounded == true) {
			/*
			if(gameObject.GetComponent<Rigidbody2D> ().IsSleeping () == true){
				gameObject.GetComponent<Rigidbody2D> ().isKinematic = true;
			}*/
		}

	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Floor"){
			grounded = true;
			//print(transform.rotation);
			/*
			float result = Mathf.Abs(transform.position.x - coll.transform.position.x);
			Renderer renderer = gameObject.GetComponent<Renderer>();
			float width = renderer.bounds.size.x;
			print (width);
			print (result);
			if(result > (width * 0.4)){
				print("perdi");
			}
			/*
			float result = Mathf.Abs(transform.position.x - coll.transform.position.x);
			print (result);
			print (transform.lossyScale);
			*/
			//StartCoroutine (DelayCollision (0.2f));
			//transform.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
			//transform.parent.gameObject.GetComponent<Build>().CheckHigh();
		}
	}

	public void Check(){
		if (transform.position.y < (limit)){
			transform.parent.gameObject.GetComponent<Build> ().floorFall ();
		}
	}

	/* delay the crane reload */
	private IEnumerator DelayCollision(float time) 
	{
		yield return new WaitForSeconds(time);
		gameObject.GetComponent<Rigidbody2D>().mass = 10f;
		yield return null;
	}
}
