﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	public static int floors;


	// Use this for initialization
	void Start () {
		floors = 0;
		//SetHighScore (0);

	}
	
	// Update is called once per frame
	void Update () {
 
	}

	/**
	 * Set the Current floors calue
	 */
	public static void SetFloors(int value){
		floors = value;
	}

	/**
	 * Increase the current floors score
	 */
	public static void AddFloor(){
		floors += 1;
	}

	/**
	 * Set the high score point
	 */
	public static void SetHighScore(int value){
		PlayerPrefs.SetInt("highScore", value);
	}
	
	/**
	 * Set the high score position
	 */
	public static void SetHighPosition(float value){
		PlayerPrefs.SetFloat("highPosition", value);
	}
	
	/**
	 * Get the high score point
	 */
	public static int GetHighScore(){
		return PlayerPrefs.GetInt("highScore");
	}
	
	/**
	 * Get the high score position
	 */
	public static float GetHighPosition(){
		return PlayerPrefs.GetFloat("highPosition");
	}

	/*
	public static void Retry(){
		floors = 0;
	}
	*/
}
