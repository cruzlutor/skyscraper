﻿using UnityEngine;
using System.Collections;

public class ParallaxLayer : MonoBehaviour {

	private Vector3 startPosition;
	public float speed;
	public bool active;

	// Use this for initialization
	void Start () {
		startPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (active == true) {
			transform.Translate(new Vector2(transform.position.x, (1 * speed * Time.deltaTime)));
		}
	}

	public void Retry() {
		transform.position = startPosition;
	}

}
